/// <reference path="../js/mvc.js" />

(function (exports) {

    function Translation() {
    }
    MVC.inheritFromBo(Translation.prototype);

    var ctor = function (args) {
        ///<summary>Creates a new isntance of the Translation class.</summary>
        ///<param name = "args">An object with the attributes of the new instance.</param>
        var o = new Translation();
        var defaults = {
            id: -1,
            name: "",
            rank: -1,
            selected: false,
            defId: -1,
            author: ''
        };
        MVC.initializeBo(o, defaults, args);
        return o;
    }
    exports.createTranslation = ctor;
})(window.MVC);

// Collection 
(function (exports) {
    function TranslationList() {
        this.itemName = "Translation";
        this.name = 'TranslationList';
    }
    MVC.inheritFromBoCollection(TranslationList.prototype);

    TranslationList.prototype.assignRanks = function() {
        ///<summary>Saves the new position of translations.</summary>
        for(var i = 0; i < this.list.length; i++) {
            this.list[i].setAttr('rank', i + 1);
        }
    }
    TranslationList.prototype.doSave = function(arr, callback) {
        ///<summary>Saves the new position of translations.</summary>
        this.list = arr;
        var s = this.serializeIds();
        var w = MVC.getActiveItem('WordList');
        var that = this;
        this.save({data: s, wordid: w.getAttr('id')}, [function() {that.assignRanks();}, callback], null, true);
    }
    var ctor = function (args) {
        ///<summary>Creates a new instance of class. Arguments should be listed here.</summary>
        var o = new TranslationList();
        var defaults = {};
        MVC.initializeBoCollection(o, defaults, args);
        o.setAttr('url', '../handlers/translation.ashx');
        return o;
    };

    exports.createTranslationList = ctor;
})(window.MVC);