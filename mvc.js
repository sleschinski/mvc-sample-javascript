// makeshift MVC-like framework for application
(function (exports) {
    var _views = {};         // all views by id
    var _bos = {};           // singleton business objects

    var mvc = {
        getActiveItem: function (collectionClassName) {
            ///<summary>Returns an active instance of the corresponding class from appropriate collection.</summary>
            ///<param name = "name">Name of the class to be retrieved (case-senistive).</param>
            ///<returns>Null if active item not set or not applicable.</param>
            return MVC.getBoSingleton(collectionClassName).getActiveItem();
        },
        getBoSingleton: function (className) {
            ///<summary>Gets an instance of business object singleton or create it if necessary and stores the reference.</summary>
            ///<param name = "name">Name of the class to be retrieved (case-senistive).</param>
            if (typeof _bos[className] === 'undefined') {
                if (typeof MVC["create" + className] !== 'undefined') {
                    var o = MVC["create" + className]();
                    _bos[className] = o;
                } else {
                    throw ("Invalid class name: " + className)
                }
            }
            return MVC.cast(className, _bos[className]);
        },
        inheritFromView: function (proto) {
            ///<summary>Adds view functions to the prototype of the new class.</summary>
            var o = MVC.createView();
            for (var p in o) { proto[p] = o[p]; }
        },
        inheritFromBo: function (proto) {
            ///<summary>Adds BO functions to proptotype of the new BO class.</summary>
            var o = MVC.createBO();
            for (var p in o) {
                if (typeof o[p] === 'function') {
                    proto[p] = o[p];
                }
            }
        },
        inheritFromBoCollection: function (proto) {
            ///<summary>Adds BO collection functions to proptotype of the new BO class.</summary>
            var o = MVC.createBoCollection();
            for (var p in o) {
                if (typeof o[p] === 'function') {
                    proto[p] = o[p];
                }
            }
        },
        initializeBoCollection: function (object, defaults, args) {
            ///<summary>Inititliazes collection of business objects.</summary>
            ///<param name = "object">Object to apply properties to</param>
            ///<param name = "defaults">Default values of attributes</param>
            ///<param name = "args">Values of attributes passed to the constructor.</param>
            var o = MVC.createBoCollection();
            for (var p in o) {
                if (typeof o[p] !== 'function') {
                    object[p] = o[p];
                }
            }
            for (var p in defaults) {
                object[p] = defaults[p];
            }
            for (var val in args) {
                object[val] = args[val];
            }
        },
        initializeBo: function (object, defaults, args) {
            ///<summary>Inititliazes business object.</summary>
            ///<param name = "object">Object to apply properties to</param>
            ///<param name = "defaults">Default values of attributes</param>
            ///<param name = "args">Values of attributes passed to the constructor.</param>
            var o = MVC.createBO();
            for (var p in o) {
                if (typeof o[p] !== 'function') {
                    object[p] = o[p];
                }
            }
            for (var p in defaults) {
                object[p] = defaults[p];
            }
            for (var val in args) {
                if (typeof object[val] == 'undefined') {
                    throw ("Unexpected attribute: " + val);
                }
                object[val] = args[val];
            }
        },
        showViews: function (names) {
            ///<summary>Displays specified views and hids all other views</summary>
            ///<param name = "names" type = "Array">Selectors of views to be displayed. Extended part will be displayed automatically.</param>
            this._showViews(names, 'hide');
        },
        showMoreViews: function (names) {
            ///<summary>Displays additional views without hiding current</summary>
            ///<param name = "names" type = "Array">Selectors of views to be displayed. Extended part will be displayed automatically.</param>
            this._showViews(names);
        },
        _batchLoad: function (bos, callback) {
            ///<summary>Loads a number of collections at once, and call callback when all of them are loaded.</summary>
            ///<param name = "bos" type = "Array">Array of bos.</param>
            var length = bos.length;
            var count = 0;
            var success = function () {
                count++;
                return (count == length);
            };
            MVC._progressFn('nothide');
            for (var i = 0; i < bos.length; i++) {
                bos[i].load(null, function () {
                    if (success()) {
                        MVC._hideProgressFn();
                        MVC.callback(callback);
                    }
                });
            }
        },
        _showViews: function (names, hide) {
            ///<summary>Displays the specified list of views</summary>
            ///<param name = "names" type = "Array">Selectors of views to be displayed. Extended part will be displayed automatically.</param>
            ///<param name = "hide" type = "Boolean">Indicates whether displayed views should be hidden.</param>
            if (hide) {
                this.hideViews();
            }
            for (var i = 0; i < names.length; i++) {
                if (typeof _views[names[i]] == 'undefined') {
                    throw ('View wasn\'t created.');
                }
                MVC._checkFunctions();

                var view = _views[names[i]];
                MVC._onLoadView(view, function (hide) {
                    if (!hide) {
                        var loadList = [];
                        var list = view._getDependencies();
                        for (var i = 0; i < list.length; i++) {
                            var bo = list[i];
                            if (bo && bo.isCollection() && bo.autoLoad && !bo.wasLoaded) {
                                loadList.push(bo);
                            }
                        }
                        if (loadList.length > 0) {
                            MVC._batchLoad(loadList, function () { view.show(); })
                        }
                        else {
                            view.show();
                        }
                    }
                });
            }
        },
        hideViews: function () {
            ///<summary>Hides all views</summary>
            for (var v in _views) {
                if (_views[v].canHide) {
                    $(v).hide();
                }
            }
        },
        getView: function (viewName) {
            ///<summary>Returns view view with specified name. In case there are several instances of the same
            /// view the first one will be returned.</summary>
            ///<param name = "viewName">Name of the view. Case sensitive.</param>
            for (var v in _views) {
                if (_views[v].name == viewName) {
                    return _views[v];
                }
            }
            throw ('Invalid view name: ' + viewName);
        },
        _getViewForEl: function (el) {
            ///<summary>Returns view object within which an event occured.</summary>
            var list = $(el).closest('.view');
            if (list.length > 0) {
                var id = '#' + list.attr('id');
                if (_views[id]) {
                    return _views[id];
                }
            }
            return null;
        },
        _getBoForEl: function (el) {
            ///<summary>Returns business object within which an event occured.</summary>
            var v = this._getSubViewForEl(el);
            return v ? v.bo : null;
        },
        _getSubViewForEl: function (el) {
            ///<summary>Returns subview object within which an event occured.</summary>
            var v = this._getViewForEl(el);
            if (v) {
                var viewId = $(el).closest('.subview').attr('viewId');
                return v._getSubViewById(viewId);
            }
            return null;
        },
        // View definition
        createView: function () {
            var count = 0;
            var idCount = 1;        // unique ids
            var subviews = [];      // parentName, el and render members must be implemented
            var depedencies = [];   // bos on which this view depends
            var view = {
                bo: null,
                name: 'view',           // should overriden in the child classes
                selector: '',           // with #
                ignoreForceSubs: false,      // ignores force for model redraw; 
                ignoreForceModel: false,      // ignores force for model redraw; 
                reuseSubviews: false,   // if false subviews will be removed on each _renderModel
                canHide: true,          // set to false if the view must be displayed constantly
                find: function (selector) {
                    ///<summary>Finds an element within current view.</summary>
                    return $(this.getSelector()).find(selector);
                },
                getDataSelector: function () {
                    ///<summary>Returns an element to hold collection associated with view (.data class).</summary>
                    return this.selector + " .data";
                },
                getExtSelector: function () {
                    ///<summary>Gets selector for the view extension
                    ///(in case part of subviews aren't descedants of the main node).</summary>
                    return this.selector + "ext";   // simply add 'ext' to id
                },
                getSelector: function () {
                    ///<summary>Returns selector for all parts of the view.</summary>
                    return this.selector + ", " + this.getExtSelector();
                },
                addDependency: function (bo) {
                    ///<summary>Adds bo (collection) which must be loaded before the view is shown.</summary>
                    for (var i = 0; i < depedencies.length; i++) {
                        if (depedencies[i] == bo) {
                            return;
                        }
                    }
                    depedencies.push(bo);
                },
                _getDependencies: function () {
                    ///<summary>Provides list of dependecies for inner use.</summary>
                    return depedencies;
                },
                _init: function (name, viewSelector, bo, args) {
                    ///<summary>Initializes the view. Must be called from constructor.</summary>
                    ///<param name = "name">Name of the view.</param>
                    ///<param name = "selector">Selector of the view</param>
                    ///<param name = "bo">Collection associated with view</param>
                    ///<param name = "args">Object with initialization arguments</param>
                    this.name = name;
                    this.selector = viewSelector;
                    this.setBo(bo);
                    if ($(this.selector).length == 0) {
                        throw ('Invalid view id: ' + this.selector);
                    }
                    $(this.selector).addClass('view');
                    $(this.getExtSelector()).addClass('view');   // register extension

                    MVC.copyPropsFrom(args, this);

                    _views[this.selector] = this;               // register it
                    _views[this.getExtSelector()] = this;
                },
                setBo: function (bo) {
                    ///<summary>Sets collection associated with view. Registers callback for on load notifications.</summary>
                    this.bo = bo;
                    if (bo) {
                        this.bo.addListener(this);
                        this.addDependency(bo);
                    }
                },
                show: function () {
                    ///<summary>Shows the view</summary>
                    if (this.name != 'StartView') {
                        $(this.getSelector()).show(); //fadeIn();
                    }
                    else {
                        $(this.getSelector()).show();
                    }
                    count++;
                    this.render('force');
                },
                hide: function () {
                    ///<summary>Hides the view</summary>
                    $(this.getSelector()).hide();
                },

                // subviews
                _clearSubViews: function () {
                    subviews = [];
                },
                clear: function () {
                    ///<summary>Removes all child elements and subviews associated with them</summary>
                    this._clearSubViews();
                    $(this.getDataSelector()).empty();
                },
                addSubView: function (key, el, bo) {
                    ///<summary>Adds a subview to the view</summary>
                    ///<param name = "key" type = "String">Key for grouping similar subviews</param>
                    ///<param name = "el">DOM element which represent subview.</param>
                    ///<param name = "bo">Business object bound to subview.</param>
                    var v = {
                        key: key,
                        el: el,
                        bo: bo
                    };

                    var props = ['key', 'el'];
                    var s = '';
                    for (var i = 0; i < props.length; i++) {
                        if (!v[props[i]]) {
                            s += props[i] + '\n';
                        }
                    }
                    if (s.length > 0) {
                        throw ('Subview methods not defined: \n' + s);
                    }

                    v.id = idCount++;
                    $(v.el).addClass('subview');
                    $(v.el).attr('key', v.key);
                    $(v.el).attr('viewId', v.id);    // id of business object is always stored in the view

                    subviews.push(v);
                    return v;
                },
                getSubViews: function (key) {
                    ///<summary>Returns all the subviews with specified key.</summary>
                    ///<param name = "key" type = "String">Key of subview.</param>
                    var arr = [];
                    for (var i = 0; i < subviews.length; i++) {
                        if (subviews[i] && subviews[i].key === key) {
                            arr.push(subviews[i]);
                        }
                    }
                    return arr;
                },
                markSubviewsChanged: function (key) {
                    ///<summary>Marks all subviews state as changed.</summary>
                    var views = key ? this.getSubViews(key) : subviews;
                    for (var i = 0; i < views.length; i++) {
                        views[i].bo.changed = true;
                    }
                },
                _getSubViewById: function (id) {
                    for (var j = 0; j < subviews.length; j++) {
                        if (subviews[j] && subviews[j].id && subviews[j].id == id) {
                            return subviews[j];
                        }
                    }
                },
                getSubViewForBo: function (bo) {
                    ///<summary>Returns the first subview associated with specified bo</summary>
                    for (var j = 0; j < subviews.length; j++) {
                        if (subviews[j] && subviews[j].bo == bo) {
                            return subviews[j];
                        }
                    }
                    return null;
                },
                // rendering
                render: function (force) {
                    ///<summary>Renders the view from business model. 
                    ///Calls this._renderModel, this._renderSubView and this._renderView as a part of the process.</summary>
                    ///<param name = "force">Forces complete redraw of the view even when no changes occured. 
                    ///Any value evaluated to non-null can be passed to achieve the result, for example 'force'.</param>
                    if (typeof this['_preRender'] !== 'undefined') {
                        this._preRender(this.bo);       // for some update of business object which must occur before each drawing
                    }
                    if (typeof this['_renderModel'] !== 'undefined') {
                        if (this.bo && (this.bo.changed || (force && !this.ignoreForceModel))) {
                            if (!this.reuseSubviews) { this._clearSubViews(); }
                            this._renderModel(this.bo);

                            if (this['bindAll'] !== 'undefined') {
                                this.bindAll();     // attach handlers as new DOM elements were most likely generated
                            }
                            
                            $(this.getDataSelector()).children().disableSelection();
                            
                            // better to specify this stuff in some callback, as it's implementation-specific
                            this.find("button").button();
                            this.find("button.icon").each(function () {
                                var icon = $(this).attr('icon');
                                $(this).button({ icons: { primary: icon, secondary: null} });
                            });
                            this.find(".progress").progressbar();
                        }
                        else {
                            if (this['bindAll'] !== 'undefined') {
                                this.bindAll();     // attach handlers as new DOM elements were most likely generated
                            }
                        }
                        this.find('.tooltip').tooltip({ tooltipClass: "tooltip-style" });
                    }
                    if (typeof this['_renderSubView'] !== 'undefined') {
                        for (var j = 0; j < subviews.length; j++) {
                            if (subviews[j] && subviews[j].bo) {
                                if (this.bo.changed || subviews[j].bo.changed || (force && !this.ignoreForceSubs)) {
                                    this._renderSubView(subviews[j].key, subviews[j].el, subviews[j].bo);
                                }
                            }
                        }
                        // an object can be associated with more than one subview, so remove flag only when drawing is done
                        for (var j = 0; j < subviews.length; j++) {
                            if (subviews[j] && subviews[j].bo) {
                                subviews[j].bo.changed = false;
                            }
                        }
                    }
                    if (typeof this['_renderView'] !== 'undefined') {
                        if (this.bo) {
                            this._renderView(this.bo);
                            App.updateHelp();
                        }
                    }
                    if (this.bo) {
                        this.bo.changed = false;
                    }
                    if (!App.noAutoHideProgress) {
                        App.hideProgress();
                    }
                },
                // pseudo-controller (inside view)
                bindGlobal: function (eventName, fnName) {
                    ///<summary>Binds event for the document object.</summary>
                    ///<param name = "eventName" type = "String">Name of the event to bind function to.</param>
                    ///<param name = "fnName" type = "String">Name of the function to handle event.</param>
                    var that = this;
                    $(document).unbind(eventName)
                        .bind(eventName, function (e) {
                            if (typeof that.handlers === 'undefined') {
                                throw ("Handlers not defined");
                            }
                            else if (typeof that.handlers[fnName] === 'function') {
                                that.handlers[fnName]({ view: that, args: e, handlers: that.handlers });
                            }
                        });
                },
                bind: function (selector, eventName, fnName) {
                    ///<summary>Attaches handler for particular event of the element. 
                    ///Selector of the current view is attached automatically.</summary>
                    ///<param name = "eventName" type = "String">Name of the event to bind function to.</param>
                    ///<param name = "fnName" type = "String">Name of the function to handle event.</param>
                    var that = this;
                    this.find(selector)
                        .unbind(eventName)
                        .bind(eventName, function (e) {
                            that._processEvent(this, fnName, e);
                        });
                },
                _processEvent: function (el, fnName, args) {
                    ///<summary>Redirects event to the controller.</summary>
                    ///<param name = "el">DOM element which event was triggered.</param>
                    ///<param name = "fnName" type = "String">Name of the function to handle event.</param>
                    ///<param name = "args">Standard event arguemnts generated by browser.</param>
                    if (typeof this.handlers === 'undefined') {
                        throw ("Handlers not defined");
                    }
                    else {
                        var e = {
                            view: MVC._getViewForEl(el),
                            bo: MVC._getBoForEl(el),
                            handlers: this.handlers,
                            el: el,
                            args: args
                        };
                        if (!e.view) {
                            throw ("No view is found");
                        }
                        if (typeof this.handlers[fnName] === 'function') {
                            this.handlers[fnName](e);
                        }
                    }
                }
            };
            return view;
        },
        createBoCollection: function () {
            ///<summary>Creates a collection of business objects.</summary>
            var activeItem = null;
            var list = this.createBO();
            list.list = [];
            list.wasLoaded = false;
            list.autoLoad = false;      // load function will be called automatically if wasLoaded = false
            list.fromCache = false;     // whether it was loaded from cache
            list.last = function () {
                return this.list.length > 0 ? this.list[this.list.length - 1] : null;
            };
            list.first = function () {
                return this.list.length > 0 ? this.list[0] : null;
            };
            list.clear = function () {
                ///<summary>Clears all the data without committing changes to server. On next access data will be reloaded.</summary>
                this.wasLoaded = false;
                this.list = [];
                activeItem = null;
            };
            list.itemIndex = function (item) {
                ///<summary>Returns index of item within the list or -1 if there is no such item</summary>
                for (var i = 0; i < this.list.length; i++) {
                    if (this.list[i] == item) {
                        return i;
                    }
                }
                return -1;
            }
            list.getActiveItem = function () {
                ///<summary>Returns active item within the collection.</summary>
                return activeItem;
            };
            list.setActiveItem = function (item) {
                ///<summary>Sets active item of the collection.</summary>
                activeItem = item;
            };
            list.moveActiveItem = function (next) {
                ///<summary>Moves active item to the next one or previous one in the list.</summary>
                for (var i = 0; i < this.list.length; i++) {
                    if (this.list[i] == activeItem) {
                        if (next && i < this.list.length - 1) {
                            activeItem = this.list[i + 1];
                        }
                        else if (!next && i > 0) {
                            activeItem = this.list[i - 1];
                        }
                        break;
                    }
                }
            };
            list.find = function (filter) {
                ///<summary>Gets items of list which agree with specified filter.</summary>
                ///<param name = "filter">Filter object. All the returned objects will have the same values of specified attributes.</param>
                if (!filter) {
                    return this.list;
                }
                else {
                    var arr = [];
                    for (var i = 0; i < this.list.length; i++) {
                        if (this.list[i].compare(filter)) {
                            arr.push(this.list[i]);
                        }
                    }
                    return arr;
                }
            };

            list.max = function (propName) {
                ///<summary>Returns the object with the max value of the specified property among the items</summary>
                ///<param name = "propName">Property name.</param>
                if (this.list.length == 0) {
                    return 0;
                }
                else {
                    var max = Number.MIN_VALUE;
                    for (var i = 0; i < this.list.length; i++) {
                        var val = this.list[i].getAttr(propName);
                        if (val > max) {
                            max = val;
                        }
                    }
                    return max;
                }
            },
            list.maxFnValue = function (fnName) {
                ///<summary>Gets the object with the largest value returned by the function.</summary>
                if (this.list.length == 0) {
                    return 0;
                }
                else {
                    var max = Number.MIN_VALUE;
                    for (var i = 0; i < this.list.length; i++) {
                        if (typeof this.list[i][fnName] !== 'function') {
                            throw ("Function not found: " + fnName);
                        }
                        var val = this.list[i][fnName]();
                        if (val > max) {
                            max = val;
                        }
                    }
                    return max;
                }
            };
            list.getUniqueValues = function (propName) {
                ///<summary>Returns string array with unique values of specifies property.</summary>
                var res = {};
                for (var i = 0; i < this.list.length; i++) {
                    if (typeof this.list[i][propName] != 'undefined') {
                        res[this.list[i][propName]] = '';  // don't store anything, we need just to add properties
                    }
                }
                var arr = [];
                for (var p in res) {
                    arr.push(p);
                }
                return arr;
            },
            list.getPropValues = function (propName) {
                ///<summary>Returns array with values of specified property.</summary>
                ///<param name = "propName">Name of the property</param>
                var arr = [];
                for (var i = 0; i < this.list.length; i++) {
                    arr.push(this.list[i][propName]);
                }
                return arr;
            },
            list.setItemAttr = function (attr, value, filter) {
                ///<summary>Sets attributes of individual items selected by filter.</summary>
                ///<param name = "attr">Name of attribute.</param>
                ///<param name = "value">Value of attribute.</param>
                ///<param name = "filter">Object with filtering expression (see find() method).</param>
                var arr = this.find(filter);
                for (var i = 0; i < arr.length; i++) {
                    arr[i].setAttr(attr, value);
                }
            };
            list.removeById = function (id, callback) {
                ///<summary>Removes object with specified id from the list and reloads the list</summary>
                var that = this;
                $.ajax({
                    type: "POST",
                    url: that.url,
                    data: { id: id, action: "delete" },
                    success: function (response) {
                        that._parseLoaded(response, true);
                        that._fireLoaded(id, callback, response);
                    }
                });
            },
            list.remove = function (filter, callback) {
                ///<summary>Removes specified items from the array</summary>
                ///<param name = "filter">Object with filtering expression.</param>
                ///<param name = "callback">Callback function.</param>

                var virtual = (typeof filter['virtual'] != 'undefined' && filter['virtual']);   // updates must not be send on the server

                delete filter["virtual"];
                var ids = this.serializeIds(filter);

                for (var i = this.list.length - 1; i >= 0; i--) {
                    if (this.list[i].compare(filter)) {
                        this.list.splice(i, 1);
                        this.changed = true;
                    }
                }

                if (!virtual && ids) {
                    $.ajax({
                        type: "POST",
                        url: this.url,
                        data: { ids: ids, action: "delete" },
                        success: function (response) {
                            MVC.callback(callback);
                        }
                    });
                }
                else {
                    MVC.callback(callback);
                }
            };
            list.add = function (params, callback) {
                ///<summary>Adds an object to the collection.</summary>
                ///<param name = "params">Params of the object.</param>
                ///<param name = "callback">Params of the object.</param>
                var that = this;
                params["action"] = "put";
                $.ajax({
                    type: "POST",
                    url: that.url,
                    data: params,
                    success: function (json) {
                        that._parseLoaded(json, false);
                        that._fireLoaded(null, callback, json);       // id = null; it won't be cached; but it's much easier this way
                    }
                });
            };
            list.parseJson = function (json, clear, id) {
                ///<summary>Parses json with list items.</summary>
                ///<param name = "json">Json string.</param>
                ///<param name = "clear">Whether the exiting items must be removed.</param>
                ///<param name = "id">Id of the parent object for the list. It is used for caching.</param>
                this._parseLoaded(json, clear);
                this._fireLoaded(id);
            }
            list._parseLoaded = function (json, clear) {
                ///<summary>Parses the request results and notify listeners (views).</summary>
                ///<param name = "json">Serialized state of the list.</param>
                ///<param name = "clear" type = "Boolean">Marks whether to clear all existing items.</param>
                if (typeof this['itemName'] === 'undefined') {
                    throw ("itemName isn't specified. Can't handle json results.")
                }
                if (clear) {
                    this.list = [];
                }
                var arr = null;
                try {
                    arr = eval('(' + json + ')');
                }
                catch (e) { }

                if (!arr) {
                    throw ("Invalid response: " + this.name + "; " + json);
                }
                if (arr) {
                    if (arr.list) {
                        arr = arr.list;
                    }
                    var ctor = "create" + this.itemName;
                    for (var i = 0; i < arr.length; i++) {
                        if (typeof MVC[ctor] === 'undefined') {
                            throw ("Can't find constructor: " + ctor);
                        }
                        var o = MVC[ctor](arr[i]);
                        this.list.push(o);
                    }
                    if (clear) {
                        // otherwise only a single item can be added, not the whole list
                        this.wasLoaded = true;
                    }
                }
            };
            list.serializeIds = function (filter) {
                ///<summary>Returns a csv string with ids of the objects.</summary>
                ///<param>Object with filter expression, for example {selected: true}</param>
                var ids = "";
                for (var i = 0; i < this.list.length; i++) {
                    if (this.list[i].compare(filter)) {
                        if (typeof this.list[i]["id"] == 'undefined') {
                            throw ("No id for the object");
                        }
                        ids += this.list[i]["id"] + ",";
                    }
                }
                return ids;
            };
            return list;
        },
        // Business object
        createBO: function () {
            var cache = {};
            var listeners = [];
            var bo = {
                changed: true,
                url: '',
                isCollection: function () {
                    ///<summary>Returns true of this object is collection.</summary>
                    return typeof this['list'] != 'undefined';
                },
                addListener: function (view) {
                    ///<summary>Adds a listener (view) which should be notified on the state change (mainly loading operations)</summary>
                    if (typeof view["render"] !== 'undefined') {
                        for (var i = 0; i < listeners.length; i++) {
                            if (listeners[i] === view) {  // prevent adding it twice
                                return true;
                            }
                        }
                        listeners.push(view);
                        return true;
                    }
                    return false;
                },
                getAttr: function (name) {
                    ///<summary>Gets attribute of bo. Throws an error if no such attribute exists.</summary>
                    if (typeof this[name] === 'undefined') {
                        throw ("Unexpected attribute requested: " + name);
                    }
                    return this[name];
                },
                setAttr: function (name, val) {
                    ///<summary>Sets the new value of attribute. Throws an error if no such attribute exists.</summary>
                    if (typeof this[name] === 'undefined') {
                        throw ("Attempt to set non-default attribute: " + name);
                    }
                    else {
                        this[name] = val;
                        this.changed = true;
                    }
                },
                compare: function (filter) {
                    ///<summary>Compares an item with the filter properties. Returns true if the values are the same.</summary>
                    ///<param name = obj>Filter object</param>
                    var t = '';
                    for (var p in filter) {
                        t = typeof this[p];
                        if ((t == 'undefined') ||
                           (t != 'function' && this[p] !== filter[p]) ||
                           (t == 'function' && this[p]() !== filter[p])) {
                            return false;
                        }
                    }
                    return true;
                },
                save: function (props, callback, noParsing, clear) {
                    ///<summary>Sends state of object to the server. Parses the returned JSON and updates the object.</summary>
                    ///<param name = "props">Array of property names to be serialized or object with parameters to be serializes. 
                    /// All properties will be serialized if ommitted.</param>
                    ///<param name = "callback">Callback function.</param>
                    ///<param name = "noParsing">Pass any value if the server response must not be evaluated into objects.</param>
                    var that = this;
                    var params = {};

                    if (Object.prototype.toString.call(props) === '[object Array]') {
                        for (var i = 0; i < props.length; i++) {
                            if (typeof this[props[i]] === 'undefined') {
                                throw ('Missing property on seralization: ' + props[i]);
                            }
                            else {
                                params[props[i]] = this[props[i]];
                            }
                        }
                    }
                    else if (typeof props == "object") {
                        params = props;
                    }
                    else {
                        throw ("Unexpected type of prop parameter (expected: object or array)");
                    }
                    params["action"] = "post";

                    var request = {
                        type: "POST",
                        url: this.url,
                        data: params,
                        success: function (json) {
                            if (!noParsing) {
                                that._parseLoaded(json, clear);
                            }
                            that._fireLoaded(null, callback, json);
                        }
                    };
                    $.ajax(request);
                },
                load: function (params, callback, notClear) {
                    ///<summary>Requests JSON from server and loads the items into the collection.</summary>
                    ///<param name = "params">An object which attributes specify parameters of request. 
                    ///Acceptable parameters must be specified in the collection.load of the child class.</param>
                    ///<param name = "callback">Callback function.</param>
                    ///<param name = "notClear">Existing items won't be cleared.</param>
                    var that = this;
                    if (!notClear) {
                        that.list = [];
                    }

                    var id = null;
                    var loaded = false;
                    var caching = params && typeof params["id"] != 'undefined';
                    if (caching) {
                        id = params["id"];
                        loaded = this._tryLoadFromCache(params["id"], callback);
                    }
                    if (!params) {
                        params = {};
                    }
                    if (typeof params["action"] == 'undefined') {
                        params["action"] = "get";
                    }

                    this.setActiveItem(null);
                    this.fromCache = loaded;

                    // if we failed to find it in cache
                    if (!loaded) {
                        var request = {
                            type: "POST",
                            url: that.url,
                            data: params,
                            success: function (json) {
                                that._parseLoaded(json, notClear ? false : true);
                                that._fireLoaded(id, callback, json);
                            }
                        };
                        $.ajax(request);
                    }
                },
                serialize: function () {
                    ///<summary>Serializes properties of the object.</summary>
                    var s = '';
                    for (var p in this) {
                        if (typeof this[p] !== 'function') {
                            var val = this[p];
                            if (val) {
                                s += p + ": " + val + '\n';
                            }
                        }
                    }
                    return s;
                },
                removeFromCache: function (id) {
                    ///<summary>Removes an item from cache.</summary>
                    if (this.hasInCache(id)) {
                        delete cache[id];
                    }
                },
                hasInCache: function (id) {
                    ///<summary>Checks whether there is anything in cache.</summary>
                    return (typeof this["list"] != "undefined" && id && typeof cache[id] != 'undefined');
                },
                _tryLoadFromCache: function (id, callback) {
                    ///<summary>Tries to load bo from cache. Returns true on success.</summary>
                    ///<param>Id of the object.</param>
                    if (id && cache[id] && typeof this["list"] != "undefined") {
                        this.list = cache[id];
                        this._fireLoaded(null, callback);
                        return true;
                    }
                    return false;
                },
                _parseLoaded: function (json) {
                    ///<summary>Parses the request results and notify listeners (views).</summary>
                    ///<param name = "json">Serialized state of the list.</param>
                    ///<param name = "callback">A function to call after loading.</param>
                    ///<param name = "id">Id of the object loaded (for caching)</param>
                    if (json && json != '') {
                        var o = eval('(' + json + ')');
                        if (o) {
                            for (var p in o) {
                                if (typeof this[p] === 'undefined') {
                                    throw ("Unexpected property: " + p);
                                }
                                this[p] = o[p];
                            }
                        }
                    }
                },
                _fireLoaded: function (id, callback, response, bo) {
                    ///<summary>Peforms caching, notifies listeners, runs callbacks.</summary>
                    if (id && typeof this["list"] != 'undefined') {
                        cache[id] = this.list;
                    }
                    this.changed = true;
                    MVC.callback(callback, response, this);   // this
                }
            }
            return bo;
        },
        _onLoadView: function () { },    // function with (view, callback) parameters to be called before view is loading
        _progressFn: function () { },
        _hideProgressFn: function () { },
        _checkFunctions: function () {
            ///<summary>Checks whether callback functions were specified correctly.</summary>
            var fns = ["_onLoadView", "_progressFn", "_hideProgressFn"];
            for (var i = 0; i < 3; i++) {
                if (typeof MVC[fns[i]] !== 'function') {
                    throw "MVC." + fns[i] + " function not defined";
                }
            }
        },
        callback: function (callback, response, bo) {
            ///<summary>Summary executes callback function or array of functions.</summary>
            if (callback) {
                if (typeof callback === 'function') {
                    callback(response, bo);
                }
                else {
                    // in case there is a list of callbacks
                    for (var i = 0; i < callback.length; i++) {
                        if (typeof callback[i] === 'function') {
                            callback[i](response, bo);
                        }
                    }
                }
            }
        },
        copyPropsFrom: function (from, to) {
            ///<summary>Copies the properties of the passed object.</summary>
            if (from && to) {
                for (var p in from) {
                    to[p] = from[p];
                }
            }
        },
        isArray: function (o) {
            ///<summary>Returns true if the object is array.</summary>
            return Object.prototype.toString.call(o) === '[object Array]';
        },
        cast: function (className, o) {
            ///<summary>A trick to enable Intellisense for untyped objects. Obsolete. It's faster to look at pertinent class.</summary>
            return o;
        }
    };
    exports.MVC = mvc;
    exports.App = {};
})(window);

