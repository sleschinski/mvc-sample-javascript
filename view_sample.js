/// <reference path="../js/mvc.js" />
/// <reference path="../js/app.js" />
/// <reference path="../js/dialog.js" />
/// <reference path="../js/utilities.js" />

(function (exports) {

    function TranslationView() {
        this.changed = false;
    }
    MVC.inheritFromView(TranslationView.prototype);

    var ctor = function (viewSelector, args) {
        ///<summary>Creates a new instance of the TranslationView</summary>
        var obj = new TranslationView();
        obj._init('TranslationView', viewSelector, MVC.getBoSingleton("TranslationList"), args);
        obj.generateStaticHtml();
        obj.render('force');
        return obj;
    };

    TranslationView.prototype.generateStaticHtml = function () {
        ///<summary>Generates any static html necessary for the view</summary>
        $(this.selector).empty();
        s = "";
        s += '<div class = "ui-state-default panelheader">Translations</div>';
        s += '<table class = "data"><ol></ol></table>';
        s += '<div class = "count prop"></div>';
        s += '<div style = "text-align:center">';
        s += '<button class = "btnsave prop" type = "button">' + App.s('Save') + '</button>';
        s += '<button class = "btncancel prop" type = "button">' + App.s('Cancel') + '</button>';
        s += '</div>'
        $(this.selector).append(s);
    }
    TranslationView.prototype._renderModel = function (bo) {
        ///<summary>Rendering in response to the changes in the list length.
        ///This function must not be called directly. Call View.render() instead.</summary>
        var $ol = this.find('ol');
        $ol.empty();
        var classes = ['name'];
        for (var i = 0; i < bo.list.length; i++) {
            var $li = $('<li>').appendTo($ol);
            this.addSubView('row', $li.get(0), bo.list[i]);
        }
    }
    TranslationView.prototype._renderSubView = function (key, el, bo) {
        ///<summary>Rendering in response to the changes in the list.</summary>
        ///This function must not be called directly. Call View.render() instead.</summary>
        if (key === 'row') {
            $(el).text(bo.getAttr('name') + ' (' + bo.getAttr('rank') + ')');
        }
    }
    TranslationView.prototype._renderView = function (bo) {
        ///<summary>Part of rendering which doesn't depend on the model.  
        ///This function must not be called directly. Call View.render() instead.</summary>
        var that = this;
        this.find('.btnsave').toggle(this.changed);
        this.find('.btncancel').toggle(this.changed);
    }
    TranslationView.prototype.bindAll = function () {
        ///<summary>Binds the events to the view. Functions with approriate names must be defined in handlers object below.</summary>
        this.bind('.btnsave', 'click', 'saveClicked');
        this.bind('.btncancel', 'click', 'cancelClicked');
    }
    TranslationView.prototype.handlers = {
        // e.bo         - business object associated with event
        // e.view       - view associated with event
        // e.handlers   - reference to this object in case context is changed
        // e.args       - standard event arguments object
        // e.el         - DOM element within which the event has occured
        prepare: function (e) {
            var views = e.view.getSubViews('row');
            var arr = [];
            var list = $(e.view.getSelector()).find('ol li');
            list.each(function () {
                for (var i = 0; i < views.length; i++) {
                    if (views[i].id == $(this).attr('viewid')) {
                        arr.push(views[i].bo);
                        break;
                    }
                }
            });
            return arr;
        },
        saveClicked: function (e) {
            ///<summary>Saves new position of words.</summary>
            var arr = e.handlers.prepare(e);
            App.showProgress('nothide');
            e.view.bo.doSave(arr, function () {
                e.view.changed = false;
                e.view.render('force');
                
            });
        },
        cancelClicked: function (e) {
            ///<summary>Restores the initial position</summary>
            $(e.view.selector).find('ol').sortable('cancel');
            e.view.changed = false;
            e.view.render('force');
        }
    }
    exports.createTranslationView = ctor;
})(window.MVC);

